Mkdocs Gitlab Pages CI Configuration
====================================

This repository holds Gitlab CI template which, once included, builds the MkDocs documentation artifacts and pushes it to S3.
These static files are then served by special infrastructure deployed on OKD4.

Example:

```yaml
include:
  - project: 'authoring/documentation/mkdocs-ci'
    file: 'mkdocs-gitlab-pages.gitlab-ci.yml'
```

## Configuration variables

The following [GitLab variables](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-cicd-variable-in-the-gitlab-ciyml-file) can be set to configure this MkDocs project:

| Variable name        | Default value | Description |
|----------------------|--------------:|-------------|
| `MKDOCS_VERSION`     | `'1.3.1'`     | Version for [MkDocs](https://www.mkdocs.org/) |
| `MATERIAL_VERSION`   | `'8.5.3'`     | Version for [Material plugin](https://squidfunk.github.io/mkdocs-material/) |
| `I18NSTATIC_VERSION` | `'0.47'`      | Version for [static-i18n plugin](https://github.com/ultrabug/mkdocs-static-i18n) |
